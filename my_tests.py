import copy
import os
from multiprocessing import Manager, Process, Pool

from src.methods.svm import SVM
from src.utils import accuracy_fn

import matplotlib.pyplot as plt
import numpy as np

import main
from src.data import load_data
from src.methods.kmeans import KMeans
from src.methods.logistic_regression import LogisticRegression


def worker(a, kmeans, data):
    centers, predicted = kmeans.k_means(data)
    d = []
    c = 0
    for i in centers:
        b = np.sum(np.sqrt(np.sum(np.power(data[predicted == c] - i, 2), axis=1)))
        b /= np.count_nonzero(predicted == c)
        d.append(b)
        c += 1
    a.append(np.sum(d) / len(d))


def average_within_cluster_distance(kmeans, data):
    a = []

    for n in range(6):
        worker(a, copy.deepcopy(kmeans), data.copy())
    return np.sum(a) / len(a)


def heuristics(heuristic, train, test, k, max_iters, xtrain, ytrain, xtest, ytest):
    print(f'training for k = {k}')
    kmeans = KMeans(k, max_iters)
    heuristic[k] = average_within_cluster_distance(copy.copy(kmeans), xtrain)
    train[k] = accuracy_fn(kmeans.fit(xtrain, ytrain), ytrain)
    test[k] = accuracy_fn(kmeans.predict(xtest), ytest)
    print(f'done for k = {k}')


def find_best_k(r):
    xtrain, xtest, ytrain, ytest = load_data('.\\donnees\\dataset_HASYv2\\')
    xtrain = xtrain.reshape(xtrain.shape[0], -1)
    xtest = xtest.reshape(xtest.shape[0], -1)

    max_iters = 200
    with Manager() as manager:
        heuristic = manager.dict(dict.fromkeys(r))
        train = manager.dict(dict.fromkeys(r))
        test = manager.dict(dict.fromkeys(r))

        inputs = []
        for k in r:
            inputs.append((heuristic, train, test, k, max_iters, xtrain, ytrain, xtest, ytest))

        with Pool(processes=8) as pool:
            pool.starmap(heuristics, inputs)
            pool.close()
            pool.join()

        plt.figure()
        plt.title('Accuracy of Kmeans')
        plt.xlabel('K')
        plt.ylabel('accuracy')
        plt.plot(r, test.values(), label='test')
        plt.plot(r, train.values(), label='train')
        plt.legend()
        plt.show()

        plt.figure()
        plt.title('Elbow for Kmeans')
        plt.xlabel('K')
        plt.ylabel('Average within-cluster distance')
        plt.plot(r, heuristic.values())
        plt.show()


class Args:
    def __init__(self,
                 data='./donnees/dataset_HASYv2',
                 method='dummy_classifier',
                 K=10,
                 lr=1e-5,
                 max_iters=100,
                 test=False,
                 svm_c=1.,
                 svm_kernel='linear',
                 svm_gamma=1.,
                 svm_degree=1,
                 svm_coef0=0):
        self.data = data
        self.method = method
        self.K = K
        self.lr = lr
        self.max_iters = max_iters
        self.test = test
        self.svm_c = svm_c
        self.svm_kernel = svm_kernel
        self.svm_gamma = svm_gamma
        self.svm_degree = svm_degree
        self.svm_coef0 = svm_coef0

        self.use_pca = False
        self.pca_d = 200


def kmeans(k, max_iters):
    args = Args(method='kmeans', K=k, max_iters=max_iters)
    main.main(args)


def logistic_regression(train, test, lr, max_iters, xtrain, ytrain, xtest, ytest, id):
    print(f'training for learning rate = {lr} and max_iters = {max_iters}   id : {id}')
    log_res = LogisticRegression(lr, max_iters)
    train[id] = accuracy_fn(log_res.fit(xtrain, ytrain), ytrain)
    test[id] = accuracy_fn(log_res.predict(xtest), ytest)
    print(f'Done {id}')

    # args = Args(method='logistic_regression', lr=lr, max_iters=max_iters)
    # main.main(args)


def logistic_regression_params(max_iters=None, lrs=None, p='lrs'):
    if lrs is None:
        lrs = [1e-5]
    if max_iters is None:
        max_iters = [200]
    xtrain, xtest, ytrain, ytest = load_data('.\\donnees\\dataset_HASYv2\\')
    xtrain = xtrain.reshape(xtrain.shape[0], -1)
    xtest = xtest.reshape(xtest.shape[0], -1)

    with Manager() as manager:
        train = manager.dict()
        test = manager.dict()

        inputs = []
        id = 0
        for lr in lrs:
            for max_it in max_iters:
                inputs.append((train, test, lr, max_it, xtrain, ytrain, xtest, ytest, id))
                id += 1

        with Pool(processes=12) as pool:
            pool.starmap(logistic_regression, inputs)
            pool.close()
            pool.join()
        print(lrs if p == 'lrs' else max_iters, test.values())
        plt.figure()
        plt.title(f'Accuracy with max_iters = {max_iters}')
        plt.ylabel('accuracy')
        plt.xlabel('learning rate')
        plt.plot(range(len(lrs if p == 'lrs' else max_iters)), test.values(), label='test')
        plt.plot(range(len(lrs if p == 'lrs' else max_iters)), train.values(), label='train')
        plt.legend()
        plt.show()


def svm_params():
    xtrain, xtest, ytrain, ytest = load_data('.\\donnees\\dataset_HASYv2\\')
    xtrain = xtrain.reshape(xtrain.shape[0], -1)
    xtest = xtest.reshape(xtest.shape[0], -1)


    with Manager() as manager:
        train = manager.dict(dict.fromkeys(range(len(Cs))))
        test = manager.dict(dict.fromkeys(range(len(Cs))))

        inputs = []
        id = 0

        Cs = [0.001, 0.1, 1, 10, 100, 1000]
        gamma = []
        # poly
        for C in Cs:
            for
            inputs.append((train, test, C, 'poly', 0.01, degree, 'auto', xtrain, ytrain, xtest, ytest, id))
            id += 1
        # linear

        # rbf

        # train, test, C, kernel, gamma, degree, coef0, xtrain, ytrain, xtest, ytest, id

        with Pool(processes=12) as pool:
            pool.starmap(svm, inputs)
            pool.close()
            pool.join()

        plt.figure()
        plt.title(f'Accuracy C = 10 kernel = poly gamma = 0.01 degree vary coef0 = 1')
        plt.ylabel('accuracy')
        plt.xlabel('learning rate')
        plt.plot(Cs, test.values(), label='test')
        plt.plot(Cs, train.values(), label='train')
        plt.legend()
        plt.show()


def svm(train, test, C, kernel, gamma, degree, coef0, xtrain, ytrain, xtest, ytest, id):
    print(f'training for {C}, {kernel}, {gamma}, {degree}, {coef0}, id: {id}')
    log_res = SVM(C, kernel, gamma, degree, coef0)
    train[id] = accuracy_fn(log_res.fit(xtrain, ytrain), ytrain)
    test[id] = accuracy_fn(log_res.predict(xtest), ytest)
    print(f'Done {id}')


if __name__ == "__main__":

    # svm_params()

    logistic_regression_params(range(100, 1000, 50), None, '')

    # svm_params()
